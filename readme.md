##  Behavior Tree Gazebo

In order to install and execute this project, perform the following steps:

### Install the project

- Download the installation files:

        $ git clone https://bitbucket.org/visionaerialrobotics/aerostack_installers.git ~/temp

- Run the following installation script to install the project "behavior_tree_gazebo":

        $ ~/temp/install_project_from_source.sh projects/behavior_tree_gazebo

### Execute the project

- Change directory to this project:

        $ cd $AEROSTACK_STACK/projects/behavior_tree_gazebo

- Execute the script that launches Gazebo:

        $ ./launcher_gazebo.sh

- Wait until the following window is presented:

![capture-gazebo.png](https://bitbucket.org/repo/rokr9B/images/916057309-capture-gazebo.png)

- Open a new terminal and change directory to the project:

        $ cd $AEROSTACK_STACK/projects/behavior_tree_gazebo

- Execute the script that launches the Aerostack components for this project:
 
        $ ./main_launcher.sh
